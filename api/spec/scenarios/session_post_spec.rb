

describe "POST /sessions" do
    context "login com sucesso" do
        before(:all) do
            payload = {email: "testeum@api.com", password: "naelton123"}
            @result = Sessions.new.login(payload)
        end

        it "valida status code" do
            expect(@result.code).to eql 200
        end

        it "valida id do usuário" do
            expect(@result.parsed_response["_id"].length).to eql 24
        end
    end

    # examples = [
    #     {
    #         payload: {email: "teste@api.com", password: "naelton1234"},
    #         code: 401,
    #         error: "Unauthorized"
    #     },
    #     {
    #         payload: {email: "teste444@api.com", password: "naelton1234"},
    #         code: 401,
    #         error: "Unauthorized"
    #     },
    #     {
    #         payload: {email: "", password: "naelton1234"},
    #         code: 412,
    #         error: "required email"
    #     },
    #     {
    #         payload: {password: "naelton1234"},
    #         code: 412,
    #         error: "required email"
    #     },
    #     {
    #         payload: {email: "teste@api.com", password: ""},
    #         code: 412,
    #         error: "required password"
    #     },
    #     {
    #         payload: {email: "teste@api.com"},
    #         code: 412,
    #         error: "required password"
    #     },
    # ]

    examples = Helpers::get_fixture("login")

    examples.each do |e|
        context "tentar logar" do
            before(:all) do
                @result = Sessions.new.login(e[:payload])
            end
    
            it "valida status code" do
                expect(@result.code).to eql e[:code]
            end
    
            it "valida id do usuário" do
                expect(@result.parsed_response["error"]).to eql e[:error]
            end
        end        
    end

   
end