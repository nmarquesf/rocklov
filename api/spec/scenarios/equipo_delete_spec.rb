

#encoding utf-8

describe "DELETE /equipo/{equipo_id}" do
    before(:all) do
        payload = {email: "testeum@api.com", password: "naelton123"}
        result = Sessions.new.login(payload)
        @user_id = result.parsed_response["_id"]
        puts @user_id
    end

    context "obter unico equipo" do
        before(:all) do
            payload = {
                thumbnail: Helpers::get_thumb("pedais.jpg"), 
                name: "Pedais",
                category: "Áudio e Tecnologia".force_encoding("ASCII-8BIT"),
                price: 199,
            }

            MongoDB.new.remove_equipo(payload[:name], @user_id)

            @equipo = Equipos.new.create(payload, @user_id)
            @equipo_id = @equipo.parsed_response["_id"]

            @result = Equipos.new.remove_by_id(@equipo_id, @user_id)
        end

        it "valida status code" do
            expect(@result.code).to eql 204
        end
    end
end